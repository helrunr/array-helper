#ifndef ARRAY_HELPER_ARRAY_HELPERS_H
#define ARRAY_HELPER_ARRAY_HELPERS_H

char *expandAllocatedMemory(void);
char *contractAllocatedMemory(char *arr);

#endif //ARRAY_HELPER_ARRAY_HELPERS_H
