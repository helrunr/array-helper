## Array Helper Functions ##

Two functions which enable variable length arrays using realloc().

```C
char *expandAllocatedMemorty(void);
```
This function will take any input from stdin and resize the array to accommodate
the input. The default increment of expansion is 10 bytes.

```C
char *contractAllocatedMemory(char *arr);
```
This function strips any leading blanks from an array and contracts the memory to
avoid waste.
