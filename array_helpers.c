#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "array_helpers.h"

char *expandAllocatedMemory(void) {

    /* Size of expansion */
    const size_t inc = 10;
    char *buffer = malloc(inc);
    char *c_pos = buffer;
    size_t max_len = inc;
    size_t len = 0;
    int character;

    /* malloc is unable to allocate memory */
    if (c_pos == NULL) {
        return NULL;
    }


    while(1) {
        character = fgetc(stdin);

        /* If a carriage return is found, return */
        if (character == '\n') {
            break;
        }

        /* Determine if buffer size has been exceeded */
        if (++len >= len) {
            /*... if so create a new block of memory */
            char *re_buf = realloc(buffer, max_len += inc);

            /* If memory cannot be reallocated return NULL */
            if (re_buf == NULL) {
                free(buffer);
                return NULL;
            }

            /* Adjust memory position to the right inside re_buf */
            c_pos = re_buf + (c_pos - buffer);
            buffer = re_buf;
        }

        /* Finish reading */
        *c_pos++ = character;
    }

    /* Add terminator */
    *c_pos ='\0';

    return buffer;
}

char *contractAllocatedMemory(char *arr) {

    char *initial = arr;
    char *new = arr;

    /* Skip over leading blanks */
    while (*initial == ' ') {
        initial++;
    }

    /* Copy remaining characters to the beginning of the string */
    while (*initial) {
        *(new++) = *(initial++);
    }

    *new = 0;

    /* reallocate memory based on the new string */
    return (char*) realloc(arr, strlen(arr) + 1);
}
